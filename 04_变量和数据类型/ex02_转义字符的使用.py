#! usr/bin/python3
# -*- coding:UTF-8 -*-

"""
mingzhang
2018/4/28
ex002_转义字符的使用.py

'\'、'r'转义字符的使用
"""


def main():
    """主函数"""

    # 当你想输出一个文件路径时会发现，'\n','\t'都不能正常输出
    # 这时需要添加转义字符告诉计算机这里的'\n'和'\t'正常输出
    print('C:\new\time')
    print('*' * 20)  # 分割线，区分上下两次结果

    # 使用转义字符 '\' 输出结果
    print('C:\\new\\time')

    # 使用转义字符 'r' 输出结果
    print(r'C:\new\time')
    print('*' * 20)
    
    """
    需要特别注意：
    '\n'是换行，'\t'是制表符，'\''是单引号
    '\"'是双引号，'\r'回车除此之外还有很多，大家用到的时候可以再查
    """
    # '\n'换行代码演示
    print('I\nLove\nPython')
    print('*' * 20)

    # '\t'制表符代码演示
    print('I\tLove\tPython')
    print('*' * 20)

    # '\''代码演示
    print('I\'m a girl')
    print('*' * 20)


if __name__ == "__main__":
    main()
