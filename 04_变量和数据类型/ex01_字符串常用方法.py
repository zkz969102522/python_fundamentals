#! usr/bin/python3
# -*- coding:UTF-8 -*-

"""
mingzhang
2018/4/28
ex001_字符串常用方法.py

调用字符串中常用的一些方法，例如：首字母大写、全部转换成大写/小写、合并字符串等
"""


def main():
    """主函数 """

    # 把字符串 "hello python world！" 存储在变量name中
    name = "Hello python world!"

    # 对变量 name 执行 title() 方法，将单词首字母转换成大写
    print(name.title())

    # 对变量 name 执行 upper() 方法，将单词所有小写字母转换成大写
    print(name.upper())

    # 对变量 name 执行 lower() 方法，将单词所有大写字母转换成小写
    print(name.lower())

    # 定义三个字符串a、b、c，使用 "+" 进行字符串的拼接
    a = "I"
    b = "am"
    c = "xiaoming"
    d = a + " " + b + " " + c
    print(d)
    print("*" * 20)

    # 字符串和整型之间的链接
    age = 23
    """
    这是错误的拼接方式
    print("happy " + age + "rd Birthday")
    因为整型不能直接与字符串进行拼接，需要类型转换,把年龄转成字符串str(age)
    """
    print("happy " + str(age) + "rd Birthday")
    # 或者把字符串直接输出
    print("happy ", age, "rd Birthday")
    print("*" * 30)

    # 在字符串"Python"前后加空格
    favorite_lan = " Python "
    print("第一次输出结果：" + favorite_lan)

    # 删除字符串中最后面空格的方法，rstrip()
    print("第二次输出结果：" + favorite_lan.rstrip())

    # 删除字符串中最前面空格的方法，lstrip()
    print("第三次输出结果：" + favorite_lan.lstrip())

    # 删除字符串中左右两侧空格的方法，strip()
    print("第四次输出结果：" + favorite_lan.strip())


if __name__ == "__main__":
    main()
