#!/usr/bin/env python
# -*- coding:UTF-8 -*-
"""
liu yi
2018/4/27
ex001_for循环计算1-100累加的和.py
"""

sum = 0
# 用for循环生成变量i，从1—100
for i in range(1, 101):
    # 给sum不停加i，直到100
    sum += i
print(sum)