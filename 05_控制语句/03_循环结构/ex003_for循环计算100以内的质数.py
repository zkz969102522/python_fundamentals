#!/usr/bin/env python
# -*- coding:UTF-8 -*-
"""
liu yi
2018/4/27
ex003_for循环计算100以内的质数.py

质数是指只能被1和自身整除的数字
"""

# 用for循环生成1 - 100 的数字
for i in range(1, 100):
    for k in range(2, i):
        # 计算从2到 i-1中有没有能够整除的数字，如果出现则不是质数，跳出内层循环取下一个数字
        if i % k == 0:
            break
    # 只有当内层循环正常结束，也就是说2到i-1没有能够被i整除的数字即为质数
    else:
        print(i, "是质数")
