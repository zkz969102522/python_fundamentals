#!/usr/bin/env python
# -*- coding:UTF-8 -*-
"""
liu yi
2018/4/28
ex004.py
"""


import random
# 生成一个随机的整数 1——100
num = random.randint(1, 101)
# 变量n用来记录猜的次数
n = 1
while True:
    user_num = int(input("请猜一下我想的数字是什么？(1 - 100)："))
    # 如果猜对了，输出恭喜，并停止循环
    if user_num == num:
        print("恭喜你猜对了，共猜了 %s 次" % n)
        break
    # 如果没猜对，则判断猜大了还是猜小了，并告知用户，同时将猜测次数加1
    else:
        if user_num > num:
            print("猜大了，请继续")
        else:
            print("猜小了，请继续")
        n += 1
