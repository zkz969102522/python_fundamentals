#!/usr/bin/env python
# -*- coding:UTF-8 -*-
"""
liu yi
2018/4/28
ex004_if判断平年和闰年.py
"""

year = int(input("输入一个年份: "))
if (year % 4) == 0:
   if (year % 100) == 0:
       if (year % 400) == 0:
           print("{0}年 是闰年".format(year))   # 整百年能被400整除的是闰年
       else:
           print("{0}年 不是闰年".format(year))
   else:
       print("{0}年 是闰年".format(year))       # 非整百年能被4整除的为闰年
else:
   print("{0}年 不是闰年".format(year))

