#! /usr/bin/python3
# -*- coding:UTF-8 -*-

"""
mingzhang
2018/4/27
ex001_判断奇数偶数.py

使用 if_else 语句判断一个数是奇数还是偶数
"""


def main():
    """主函数 """
    n = 33

    if n % 2 == 0:  # 判断整数 n 是否能够被 2 整除
        print(n, "是偶数")
    else:
        print(n, "是奇数")


if __name__ == "__main__":
    main()
