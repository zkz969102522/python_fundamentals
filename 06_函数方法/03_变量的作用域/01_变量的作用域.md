# 01_变量的作用域

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号：TLV_CN
>
> 服务号：TEDU_TMOOC

---

<!-- TOC -->

- [01_变量的作用域](#01)
    - [1. 变量作用域](#1)
        - [1.1. 概念](#11)
        - [1.2. 四个作用域](#12)
        - [1.3. 局部作用域和全局作用域](#13)
        - [1.4. global和nonlocal关键字](#14-globalnonlocal)

<!-- /TOC -->

## 1. 变量作用域

### 1.1. 概念

作用域也叫命名空间，是变量访问的时候查找的范围空间，在程序中变量并不是在每个位置都能访问，作用域决定了这个变量在哪里可以被访问到

### 1.2. 四个作用域

- L (Local) 局部作用域（函数内）
- E (Enclosing) 外部嵌套函数作用域
- G (Global) 全局作用域
- B (Built-inython) 内置模块的作用域

下面通过一段代码简单说明一下作用域

```python
def funa():
    # 外部函数嵌套作用域
    a = 1
    def funb():
        # 局部作用域
        b = 2
        # 局部作用域中没有定义a，程序会去外部找
        c = a * b
        print(c)
    return funb

# 全局作用域
d = 3
```

变量在作用域中的查找范围是：局部作用域（函数内） > 外部嵌套函数作用域 > 全局作用域 > 内置模块的作用域，程序访问变量时先在局部局部作用域中查找，找不到的话，如果该函数嵌套在另一个函数之中，则会去外部嵌套函数作用域中查找，如果还是找不到则去全局作用域中寻找，最后在查找内置模块作用域

### 1.3. 局部作用域和全局作用域

在函数内部定义的变量的作用范围仅限于函数内部，一起来看一下面的代码：

```python
def funa():
    # 局部作用域，a为局部变量
    a = 1

# 全局作用域，全局作用域中并没有定义a
print(a + 1)
```

在上面的代码中程序会抛出一个异常，告诉我们a变量找不到，原因就是我们在函数内部定义的变量a属于局部作用域，他的访问范围仅限于函数内部，所以我们在函数外部尝试访问这个变量，则会产生异常

### 1.4. global和nonlocal关键字

在局部作用域中可以访问全局变量，但是需要修改全局变量时则需要用global关键字声明

```python
def funa():
    # 局部作用域中访问全局变量
    print(a + 1)

# 全局变量
a = 1
funa()
```

代码中虽然在局部作用中没有定义变量a，但是根据查找顺序还是能访问到全局变量a，所以程序并不会抛异常

```python
def funa():
    # 局部作用域，在局部作用域中修改全局变量a
    # 声明a为全局变量a
    global a
    a += 1

# 全局变量a
a = 1
funa()
```

