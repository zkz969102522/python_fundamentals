"""微软 Access 中的 Northwin数据库 改造

northwind.json 是json 格式的产品信息, 包含79个产品, 产品信息如下所示

"ProductId": 产品编号
"ProductName": 产品名称
"SupplierId": 供应商编号
"SupplierName": 供应商名称
"CategoryId": 类别编号
"CategoryName": 类别名称
"UnitNum": 规格描述
"UnitPrice": 产品单价
"StockNum": 库存量
"OrderNum": 订购量
"ReOrderNum": 续订量
"IsAbord": 是否终止供应, True 为终止供应, False 为正常供应

northwind_model.py 文件用于定义产品类, 包含 json 文件中的所有产品属性

northwind_helper.py 文件定义了读写 jsong 文件的方法, 同时将 json 格式的产品信息转换成列表

"""

# 使用 from tlv_00_公共模块.northwind import * 语句时, 将导入下列模块
__all__ = ['northwind_helper', 'northwind_model']
